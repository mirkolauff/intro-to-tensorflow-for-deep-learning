## [Intro to TensorFlow for Deep Learning](https://classroom.udacity.com/courses/ud187)

This is the 2019 course covering Deep Learning with Tensorflow 2.0

### Requirements
- [Pyenv](https://github.com/pyenv/pyenv)
- [Pipenv](https://github.com/pypa/pipenv)
- Python 3.6.8

### How to start
- Install Python 3.6.8 for Pyenv, if not already installed:

        $ pyenv install 3.6.8

- Create virtual environment:

        $ pipenv install

- Enter environment:

        $ pipenv shell

- Start a notebook locally (from inside virtual environment):

        $  jupyter-notebook <sub_dir>/<notebook_name.ipynb>

### Content
This notebook contains the modified jupyter notebooks from the course, so they might be run locally in the corresponding `lesson_X` folder.
Additionally this repo contains a directory called `models`, which contains the already trained models of the corresponding lessons, which can be reloaded with:

      $ model.load(<path to model folder>)

### Notes
**2019-11-05**: The models for the notebooks `08_18`, `08_21`, `08_24` and `08_30` cannot be saved with `model.save`, because a bug causes an AssertionError in `Tensorflow 2.0.0`. See [here](https://github.com/tensorflow/tensorflow/issues/27711) and [here](https://github.com/tensorflow/tensorflow/issues/31686). This issue should be solved in the next update and already solved in version `2.0.0-dev20190416`.
